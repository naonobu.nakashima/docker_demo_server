from flask import Flask, request, json, jsonify, render_template
from datetime import datetime
from pytz import timezone
from pymongo import MongoClient
import json
import ast
from bson import json_util
from bson.objectid import ObjectId
import time, random
from flask_socketio import SocketIO, emit, send, Namespace
import os

client = MongoClient("mongodb://db:27017")
db = client.projectDB
db_usage = db["usage"]

async_mode = None

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
#socketio.init_app(app, cors_allowed_origins="*")

tmp_time = datetime.now(timezone('Asia/Tokyo'))
tmp_cpu = 0 
tmp_mem = 0


@app.route('/hello')
def hello_world():
    return jsonify({'message': 'Hello World'})

@app.route('/list', methods=['GET'])
def view_test():
    return render_template('vue_test.html')


@app.route('/usage', methods=['GET'])
def usage_list():
    corsor = db_usage.find().sort([('time', -1)]).limit(20)
    #rtn_msg = []
    rtn_msg = {}
    for doc in corsor :
        json_doc = ast.literal_eval(json.dumps(doc, default=json_util.default))
        print(type(json_doc["time"]))
        print(json_doc["_id"])
        print(json_doc['hostname'])
        tmp_dict = {
            json_doc["_id"]["$oid"]: {
                'time': json_doc['time'],
                'hostname': json_doc['hostname'],
                'cpu': json_doc['cpu'],
                'mem': json_doc['mem'] 
            }
        }
        print(tmp_dict)
        rtn_msg.update(tmp_dict)
    return jsonify(rtn_msg)

@app.route('/usage/<string:hname>', methods=['GET'])
def usage_detail(hname):
    corsor = db_usage.find(filter={'hostname':{'$regex':hname}}).sort([('time', -1)]).limit(20)
    #rtn_msg = []
    rtn_msg = {}
    for doc in corsor :
        json_doc = ast.literal_eval(json.dumps(doc, default=json_util.default))
        print(type(json_doc["time"]))
        print(json_doc["_id"])
        print(json_doc['hostname'])
        tmp_dict = {
            json_doc["_id"]["$oid"]: {
                'time': json_doc['time'],
                'hostname': json_doc['hostname'],
                'cpu': json_doc['cpu'],
                'mem': json_doc['mem'] 
            }
        }
        print(tmp_dict)
        rtn_msg.update(tmp_dict)
    return jsonify(rtn_msg)

@app.route('/usage', methods=['POST'])
def usage_create():
    req = ast.literal_eval(request.get_json())
    print(req)
    if 'cpu' in req and 'mem' in req and 'hostname' in req and 'time' in req:
        db_usage.insert({
            'time': req['time'],
            'hostname': req['hostname'],
            'cpu': req['cpu'],
            'mem': req['mem']  
        })
        msg = 'New usage is recorded.'
    else:
        msg = 'No usage is recorded.'
    rtn_msg  = {
        'message': msg,
        'data': {
            'time': req['time'],
            'hostname': req['hostname'],
            'cpu': req['cpu'],
            'mem': req['mem']            
        }
    }

    if req['hostname'] == os.uname()[1]:
        t = int(time.mktime(datetime.fromisoformat(req['time']).timetuple()))
        socketio.emit('containerdata', json.dumps([{"time": t, "y": req['cpu']}, {"time": t, "y": req['mem']}]), namespace="/senddata")

    return jsonify(rtn_msg )
    

@app.route('/usage/<int:id>', methods=['DELETE'])
def usage_delete(id):
    pass

def background():
    print('Send')
    while True:
        socketio.sleep(1) 
        t = int(time.mktime(datetime.now().timetuple()))
        socketio.emit('my_response', json.dumps([{"time": t, "y": random.random() * 1000},{"time": t, "y": random.random() * 1000}]), namespace="/test")
        print('emitted')
    return

@app.route('/')
def index():
    socketio.start_background_task(target=background)
    return render_template('index.html')

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000, debug=True)