# Outline

* The containers created from the docker-compose.yml execute 2 python programs.
* One of python program is server which recieve the cpu & mem data sent from client.py on a container, and shows the data in list or graph style.
* Another one is send own container cpu & mem usage to the sever. 

# How to use

1. Git clone.
    ~~~
    > git clone https://gitlab.com/naonobu.nakashima/docker_demo_server.git
    ~~~
1. Execute server program:app.py.
   1. Docker-compose.
       ~~~
       > cd docker_demo_server
       > docker-compose up -d
       ~~~
1. Execute client program:client.py on the app container:docker_demo_server_app_1.
   1. Set the hostname which you want to run the docker containers, to `SERVER_HOSTNAME` in .env.app file.
        ~~~
        SERVER_HOSTNAME=yourhostname.abc.local
        ~~~
   1. Be sure to configure Docker DNS settings to be able to resolve the hostname above on your docker container. 
   1. You can run client.py by below command:
        ~~~
        > docker exec -it docker_demo_server_app_1 python /workspace/client.py
        ~~~
1. Check the recieved cpu & mem usage.
   1. LIST: Access `http://SERVER_HOSTNAME:5000/list` by web browser, then you can check the cpu & mem usage sended from client.py.
   2. GRAPH: Access `http://SERVER_HOSTNAME:5000/` by web browser, then you can check the graph of cpu & mem usage sended from client.py on container:docker_demo_server_app_1.